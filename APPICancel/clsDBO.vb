﻿Imports System.Threading
Imports System.Data.SqlClient
Imports Microsoft.Win32.SafeHandles
Imports System.Runtime.InteropServices

Public Class clsDBO : Implements IDisposable
    Private sSQL As String
    Private ds As DataSet
    Private dr As DataRow
    ' Flag: Has Dispose already been called?
    Dim disposed As Boolean = False
    ' Instantiate a SafeHandle instance.
    Dim handle As SafeHandle = New SafeFileHandle(IntPtr.Zero, True)
    Private sConnectionString As String

    Public Sub AddRow()
        ds.Tables(0).Rows.Add(dr)
    End Sub

    Public Sub GetRowNo(ByVal xNo As Long)
        dr = ds.Tables(0).Rows(xNo)
    End Sub

    Public Sub GetRow()
        dr = ds.Tables(0).Rows(0)
    End Sub

    Public Sub NewRow()
        dr = ds.Tables(0).NewRow
    End Sub

    Public Property Fields(xFieldName As String) As String
        Get
            If Not IsDBNull(dr(xFieldName)) Then
                Fields = dr(xFieldName)
            Else
                Fields = ""
            End If
        End Get
        Set(value As String)
            If value.Length > 0 Then
                value = value.Replace("'", "")
                dr(xFieldName) = value
            Else
                dr(xFieldName) = DBNull.Value
            End If
        End Set
    End Property

    Public Property FieldsWith1quote(xFieldName As String) As String
        Get
            If Not IsDBNull(dr(xFieldName)) Then
                FieldsWith1quote = dr(xFieldName)
            Else
                FieldsWith1quote = ""
            End If
        End Get
        Set(value As String)
            If value.Length > 0 Then
                dr(xFieldName) = value
            Else
                dr(xFieldName) = DBNull.Value
            End If
        End Set
    End Property

    Public ReadOnly Property RowCount As Long
        Get
            RowCount = ds.Tables(0).Rows.Count
        End Get
    End Property

    Public Function GetData(xSQL As String, xConnectionString As String) As DataSet
        Try
            ds = New DataSet
            sSQL = xSQL
            Dim con As New SqlConnection(xConnectionString)
            Dim da As New SqlDataAdapter(sSQL, con)
            da.Fill(ds, "DBO")
            da.Dispose()
            con.Close()
            con.Dispose()
        Catch ex As Exception
            Dim sTemp As String
            sTemp = ex.Message
        End Try
        GetData = ds
    End Function

    Public Sub OpenDB(xSQL As String, xConnectionString As String)
        Try
MoveHere:
            ds = New DataSet
            sSQL = xSQL
            sConnectionString = xConnectionString
            Dim con As New SqlConnection(sConnectionString)
            Dim da As New SqlDataAdapter(sSQL, con)
            da.Fill(ds, "DBO")
            da.Dispose()
            con.Close()
            con.Dispose()
        Catch ex As Exception
            Dim sTemp As String
            sTemp = ex.Message
            If InStr(sTemp.ToLower, "restore", CompareMethod.Text) > 0 Then
                System.Threading.Thread.Sleep(60000)
                GoTo MoveHere
            End If
        End Try
    End Sub

    Public Sub RunSQL(xSQL As String, xConnectionString As String)
        Try
            Dim con As New SqlConnection(xConnectionString)
            con.Open()
            Dim cmd As New SqlCommand
            cmd.Connection = con
            cmd.CommandText = xSQL
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Dim sTemp As String
            sTemp = ex.Message
        End Try

    End Sub

    Public Sub SaveDB()
        Dim dsChange As DataSet = ds.GetChanges
        If dsChange Is Nothing Then
            Exit Sub
        End If

        Dim con As New SqlConnection(sConnectionString)
        Dim da As New SqlDataAdapter(sSQL, con)
        Dim cb As New SqlCommandBuilder(da)
        da.Update(dsChange, "DBO")
        dsChange.Dispose()
        da.Dispose()
        ds.Dispose()
        cb.Dispose()
        con.Close()
        con.Dispose()
        con = Nothing
        cb = Nothing
        ds = Nothing
        dr = Nothing
        da = Nothing
        dsChange = Nothing
    End Sub

    Public Sub Dispose() _
           Implements IDisposable.Dispose
        ' Dispose of unmanaged resources.
        Dispose(True)
        ' Suppress finalization.
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If disposed Then Return

        If disposing Then
            handle.Dispose()
            ' Free any other managed objects here.
            '
        End If

        ' Free any unmanaged objects here.
        '
        disposed = True
    End Sub
End Class
